#Read Me - Disclaimer

This software must be used only for research purpose. In order to avoid its diffusion, the software folder is protected by a password.
For obtaining the password, you must send an email to **avola [at] di.uniroma1.it** containing your name, lastname, email address and institution.

The submission of the email to require the password entails the acceptance of the following terms:
The information provided in the email must be authentic;
The access permission to the software is only given to the users specified in the email;
Do not to distribute the software to other parties without written permission;
Do not to use the software for commercial purposes.

Upon receipt and approval of your request, we will send you the password as soon as possible. We will not respond to an invalid request.
The information of your request will remain private and never will be distributed to other parties.

**IMPORTANT: please, copy and paste this disclaimer information within the email when you send the request of the password.**